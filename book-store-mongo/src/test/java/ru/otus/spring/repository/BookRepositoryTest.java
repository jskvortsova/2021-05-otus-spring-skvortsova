package ru.otus.spring.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.exception.BookException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
class BookRepositoryTest {
    public static final String EXPECTED_BOOK_NAME = "Гарри Поттер";
    public static final int EXPECTED_COUNT = 1;
    public static final String BOOK_1_ID = "book1id";
    public static final String EXPECTED_GENRE = "приключения";
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    void count() {
        assertEquals(EXPECTED_COUNT, bookRepository.count());
    }

    @Test
    void getAll() {
        final List<Book> all = bookRepository.findAll();
        assertEquals(EXPECTED_COUNT, all.size());
        final Book book = all.get(0);
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertThat(book.getGenres()).contains(EXPECTED_GENRE);
    }

    @Test
    void findById() {
        final Optional<Book> bookOpt = bookRepository.findById(BOOK_1_ID);
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertThat(book.getGenres()).contains(EXPECTED_GENRE);
    }

    @Test
    void findByName() {
        final Optional<Book> bookOpt = bookRepository.findByName(EXPECTED_BOOK_NAME);
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertThat(book.getGenres()).contains(EXPECTED_GENRE);
    }

    @Test
    void findByAuthorId() {
        final List<Book> books = bookRepository.findByAuthorName("Джоан Роулинг");
        assertEquals(1, books.size());
        final Book book = books.get(0);
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertThat(book.getGenres()).contains(EXPECTED_GENRE);

    }

    @Test
    void findByGenreId() {
        final List<Book> books = bookRepository.findByGenre(EXPECTED_GENRE);
        assertEquals(1, books.size());
        final Book book = books.get(0);
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertThat(book.getGenres()).contains(EXPECTED_GENRE);
    }

    @Test
    void insert() {
        assertEquals(EXPECTED_COUNT, bookRepository.count());
        final Book book = new Book().setName("New book").setDescription("New book description")
                .setGenres(Collections.singletonList(EXPECTED_GENRE));
        bookRepository.save(book);
        assertNotNull(book.getId());
        assertEquals(EXPECTED_COUNT + 1, bookRepository.count());
        final Optional<Book> newBookOpt = bookRepository.findById(book.getId());
        assertTrue(newBookOpt.isPresent());
        final Book newBook = newBookOpt.get();
        assertThat(book.getGenres()).contains(EXPECTED_GENRE);
        assertEquals("New book", newBook.getName());
        // rollback
        bookRepository.delete(book);
    }

    @Test
    void deleteById() throws BookException {
        assertEquals(EXPECTED_COUNT, bookRepository.count());
        final List<Comment> commentsBeforeDeletion = getCommentsByBookId(BOOK_1_ID);
        assertThat(commentsBeforeDeletion).isNotEmpty();
        final Optional<Book> book = bookRepository.findById(BOOK_1_ID);
        assertThat(book).isPresent();
        bookRepository.deleteById(BOOK_1_ID);
        assertEquals(EXPECTED_COUNT - 1, bookRepository.count());
        final Optional<Book> deleted = bookRepository.findById(BOOK_1_ID);
        assertThat(deleted).isEmpty();
        final List<Comment> commentsAfterDeletion = getCommentsByBookId(BOOK_1_ID);
        assertThat(commentsAfterDeletion).isEmpty();
    }

    private List<Comment> getCommentsByBookId(String bookId) {
        final Query query = new Query();
        query.addCriteria(Criteria.where("id").is(bookId));
        final Book book = mongoTemplate.findOne(query, Book.class);
        return Optional.ofNullable(book).map(Book::getComments).orElse(Collections.emptyList());
    }
}