package ru.otus.spring.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.otus.spring.entity.Author;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.repository.BookRepository;
import ru.otus.spring.service.AuthorService;
import ru.otus.spring.service.BookService;
import ru.otus.spring.service.GenreService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BookServiceImplTest {
    private static final long EXPECTED_COUNT = 1;
    public static final String BOOK_1_ID = "book1id";
    public static final String EXPECTED_BOOK_NAME = "Гарри Поттер";
    public static final String AUTHOR_NAME = "Джоан Роулинг";
    public static final String GENRE = "приключения";
    private BookService bookService;
    private List<Book> expectedBooks;

    @BeforeEach
    void setup() {
        final BookRepository bookRepository = mock(BookRepository.class);
        final GenreService genreService = mock(GenreService.class);
        final AuthorService authorService = mock(AuthorService.class);
        bookService = new BookServiceImpl(bookRepository, genreService, authorService);

        final Book book = new Book();
        book.setId(BOOK_1_ID);
        book.setName(EXPECTED_BOOK_NAME);
        book.setDescription("description");
        book.setAuthors(Collections.singletonList(new Author(AUTHOR_NAME)));
        book.setGenres(Collections.singletonList(GENRE));

        expectedBooks = Collections.singletonList(book);
        when(bookRepository.findAll()).thenReturn(expectedBooks);
        when(bookRepository.findById(BOOK_1_ID)).thenReturn(Optional.of(book));
        when(bookRepository.findByAuthorName(AUTHOR_NAME)).thenReturn(expectedBooks);
        when(bookRepository.findByGenre(GENRE)).thenReturn(expectedBooks);
        when(bookRepository.findByName(EXPECTED_BOOK_NAME)).thenReturn(Optional.of(book));
        when(bookService.count()).thenReturn(EXPECTED_COUNT);
        when(genreService.findById("1")).thenReturn(Optional.of(new Genre(GENRE)));
    }

    @Test
    void count() {
        assertEquals(EXPECTED_COUNT, bookService.count());
    }

    @Test
    void getAll() {
        final List<Book> all = bookService.getAll();
        assertEquals(EXPECTED_COUNT, all.size());
        assertThat(all).doesNotContainNull();
        assertThat(all).isSameAs(expectedBooks);
    }

    @Test
    void findById() {
        final Optional<Book> bookOpt = bookService.findById(BOOK_1_ID);
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
    }

    @Test
    void findByName() {
        final Optional<Book> bookOpt = bookService.findByName(EXPECTED_BOOK_NAME);
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
    }

    @Test
    void findByAuthorId() {
        final List<Book> books = bookService.findByAuthorName(AUTHOR_NAME);
        assertEquals(1, books.size());
        final Book book = books.get(0);
        assertNotNull(book);
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
    }

    @Test
    void findByGenreId() {
        final List<Book> books = bookService.findByGenre(GENRE);
        assertEquals(1, books.size());
        final Book book = books.get(0);
        assertNotNull(book);
        assertEquals(BOOK_1_ID, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
    }

    @Test
    void insert() {
        assertThrows(BookException.class, () -> bookService.save(null, "desc", "1"));
        assertThrows(BookException.class, () -> bookService.save("name", "desc", null));
        assertDoesNotThrow(() -> bookService.save("name", "desc", "1"));
    }

    @Test
    void testInsert() {
        assertThrows(BookException.class, () -> bookService.save(null));
        final Book book1 = new Book().setId("1").setDescription("desc").setGenres(Collections.singletonList(GENRE));
        assertThrows(BookException.class, () -> bookService.save(book1));
        final Book book2 = new Book().setId("1").setName("name").setDescription("desc");
        assertThrows(BookException.class, () -> bookService.save(book2));
        final Book book3 = new Book().setId("1").setName("name").setDescription("desc").setGenres(Collections.singletonList(GENRE));
        assertDoesNotThrow(() -> bookService.save(book3));

    }

    @Test
    void deleteById() {
        assertDoesNotThrow(() -> bookService.deleteById(BOOK_1_ID));
    }
}