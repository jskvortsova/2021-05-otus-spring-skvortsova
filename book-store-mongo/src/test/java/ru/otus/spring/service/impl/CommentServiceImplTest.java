package ru.otus.spring.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.exception.CommentException;
import ru.otus.spring.service.BookService;
import ru.otus.spring.service.CommentService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CommentServiceImplTest {

    private CommentService commentService;
    public static final String BOOK_1_ID = "book1id";

    @BeforeEach
    void setup() {
        final BookService bookService = mock(BookService.class);
        commentService = new CommentServiceImpl(bookService);

        final Book book = new Book();
        book.setId(BOOK_1_ID);
        book.setDescription("description");
        List<Comment> comments = new ArrayList<>();
        comments.add(new Comment(1L, "comment", "author"));
        book.setComments(comments);

        when(bookService.findById(BOOK_1_ID)).thenReturn(java.util.Optional.of(book));

    }

    @Test
    void getByBookId() throws BookException {
        final List<Comment> comments = commentService.getByBookId(BOOK_1_ID);
        assertNotNull(comments);
        assertEquals(1, comments.size());
    }

    @Test
    void insert() {
        assertThrows(CommentException.class, () -> commentService.insert(null, null));
        assertThrows(CommentException.class, () -> commentService.insert(new Comment(), null));
        assertThrows(CommentException.class, () -> commentService.insert(new Comment(1L, "comment", "author"), null));
        assertDoesNotThrow(() -> commentService.insert(new Comment(1L, "comment", "author"), BOOK_1_ID));
    }

    @Test
    void testInsert() {
        assertThrows(CommentException.class, () -> commentService.insert(null, "author", "comment"));
        assertThrows(CommentException.class, () -> commentService.insert(BOOK_1_ID, "author", null));
        assertDoesNotThrow(() -> commentService.insert(BOOK_1_ID, null, "comment"));
    }

    @Test
    void deleteById() {
        assertDoesNotThrow(() -> commentService.deleteById(1L, BOOK_1_ID));
    }
}