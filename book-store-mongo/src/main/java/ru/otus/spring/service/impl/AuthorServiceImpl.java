package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.otus.spring.entity.Author;
import ru.otus.spring.exception.AuthorException;
import ru.otus.spring.repository.AuthorRepository;
import ru.otus.spring.service.AuthorService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;

    @Override
    @Transactional(readOnly = true)
    public long count() {
        return authorRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Author> getAll() {
        return authorRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Author> findById(String id) {
        return authorRepository.findById(id);
    }

    @Override
    @Transactional
    public void insert(Author author) throws AuthorException {
        if (author == null) {
            throw new AuthorException("Author mustn't be null!");
        }
        if (!StringUtils.hasLength(author.getName())) {
            throw new AuthorException("Author name mustn't be null!");
        }
        authorRepository.save(author);
    }

    @Override
    @Transactional
    public void insert(String name) throws AuthorException {
        insert(new Author(name));
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        authorRepository.deleteById(id);
    }
}
