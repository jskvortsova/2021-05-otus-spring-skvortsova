package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.exception.GenreException;
import ru.otus.spring.repository.GenreRepository;
import ru.otus.spring.service.GenreService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class GenreServiceImpl implements GenreService {
    private final GenreRepository genreRepository;

    @Override
    @Transactional(readOnly = true)
    public long count() {
        return genreRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Genre> getAll() {
        return genreRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Genre> findById(String id) {
        return genreRepository.findById(id);
    }

    @Override
    @Transactional
    public void insert(Genre genre) throws GenreException {
        if (genre == null) {
            throw new GenreException("Genre mustn't be null!");
        }
        if (!StringUtils.hasLength(genre.getName())) {
            throw new GenreException("Genre name mustn't be null!");
        }
        genreRepository.save(genre);
    }

    @Override
    @Transactional
    public void insert(String name) throws GenreException {
        insert(new Genre(name));
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        genreRepository.deleteById(id);
    }
}
