package ru.otus.spring.service;

import ru.otus.spring.entity.Book;
import ru.otus.spring.exception.AuthorException;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.exception.GenreException;

import java.util.List;
import java.util.Optional;

public interface BookService {
    long count();

    List<Book> getAll();

    Optional<Book> findById(String id);

    Optional<Book> findByName(String name);

    List<Book> findByAuthorName(String authorName);

    List<Book> findByGenre(String genre);

    void save(Book book) throws BookException;

    void save(String name, String description, String genreId) throws BookException, GenreException;

    void save(String name, String description, String genreId, String authorId) throws BookException, AuthorException, GenreException;

    void deleteById(String id);

    List<Book> deleteByGenre(String genre);

    List<Book> deleteByAuthorId(String authorId);
}
