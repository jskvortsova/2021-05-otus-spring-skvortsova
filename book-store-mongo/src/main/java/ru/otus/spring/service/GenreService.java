package ru.otus.spring.service;

import ru.otus.spring.entity.Genre;
import ru.otus.spring.exception.GenreException;

import java.util.List;
import java.util.Optional;

public interface GenreService {

    long count();

    List<Genre> getAll();

    Optional<Genre> findById(String id);

    void insert(Genre genre) throws GenreException;

    void insert(String name) throws GenreException;

    void deleteById(String id);
}
