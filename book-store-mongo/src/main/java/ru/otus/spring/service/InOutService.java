package ru.otus.spring.service;

public interface InOutService {
    void write(String message);

    String read();
}
