package ru.otus.spring.service;

import ru.otus.spring.entity.Comment;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.exception.CommentException;

import java.util.List;

public interface CommentService {

    List<Comment> getByBookId(String bookId) throws BookException;

    void insert(Comment comment, String bookId) throws CommentException, BookException;

    void insert(String bookId, String author, String comment) throws CommentException, BookException;

    void deleteById(long id, String bookId) throws CommentException, BookException;
}
