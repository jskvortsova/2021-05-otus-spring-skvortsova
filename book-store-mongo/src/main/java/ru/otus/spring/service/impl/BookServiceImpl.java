package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import ru.otus.spring.entity.Author;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.repository.BookRepository;
import ru.otus.spring.service.AuthorService;
import ru.otus.spring.service.BookService;
import ru.otus.spring.service.GenreService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final GenreService genreService;
    private final AuthorService authorService;

    @Override
    @Transactional(readOnly = true)
    public long count() {
        return bookRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Book> findById(String id) {
        return bookRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Book> findByName(String name) {
        return bookRepository.findByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Book> findByAuthorName(String authorName) {
        return bookRepository.findByAuthorName(authorName);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Book> findByGenre(String genre) {
        return bookRepository.findByGenre(genre);
    }

    @Override
    @Transactional
    public void save(Book book) throws BookException {
        if (book == null) {
            throw new BookException("Book mustn't be null");
        }
        if (!StringUtils.hasLength(book.getName())) {
            throw new BookException("Name shouldn't be null or empty");
        }
        if (CollectionUtils.isEmpty(book.getGenres())) {
            throw new BookException("Genre ID shouldn't be null");
        }
        bookRepository.save(book);
    }

    @Override
    @Transactional
    public void save(String name, String description, String genreId) throws BookException {
        final List<String> genres = genreService.findById(genreId)
                .map(Genre::getName)
                .map(Collections::singletonList).orElse(null);
        final Book book = new Book()
                .setName(name)
                .setDescription(description)
                .setGenres(genres);
        save(book);
    }

    @Override
    public void save(String name, String description, String genreId, String authorId) throws BookException {
        final Author author = getAuthor(authorId);
        final String genre = getGenre(genreId).getName();

        final Book book = new Book()
                .setName(name)
                .setDescription(description)
                .setGenres(Collections.singletonList(genre))
                .setAuthors(Collections.singletonList(author));
        save(book);
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        bookRepository.deleteById(id);
    }

    @Override
    public List<Book> deleteByGenre(String genre) {
        return bookRepository.deleteByGenre(genre);
    }

    @Override
    public List<Book> deleteByAuthorId(String authorId) {
        return bookRepository.deleteByAuthorId(authorId);
    }


    private Genre getGenre(String genreId) throws BookException {
        if (genreId == null) {
            throw new BookException("Genre ID mustn't be null!");
        }
        return genreService.findById(genreId)
                .orElseThrow(() -> new BookException("Genre doesn't exist!"));
    }

    private Author getAuthor(String authorId) throws BookException {
        if (authorId == null) {
            throw new BookException("Author ID mustn't be null!");
        }
        return authorService.findById(authorId)
                .orElseThrow(() -> new BookException("Author doesn't exist!"));
    }
}
