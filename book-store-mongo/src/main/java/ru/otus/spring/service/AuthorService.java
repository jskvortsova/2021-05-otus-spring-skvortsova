package ru.otus.spring.service;

import ru.otus.spring.entity.Author;
import ru.otus.spring.exception.AuthorException;

import java.util.List;
import java.util.Optional;

public interface AuthorService {

    long count();

    List<Author> getAll();

    Optional<Author> findById(String id);

    void insert(Author author) throws AuthorException;

    void insert(String name) throws AuthorException;

    void deleteById(String id);
}
