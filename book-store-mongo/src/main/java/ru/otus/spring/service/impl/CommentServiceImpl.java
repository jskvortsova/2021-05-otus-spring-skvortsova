package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.exception.CommentException;
import ru.otus.spring.service.BookService;
import ru.otus.spring.service.CommentService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {
    private final BookService bookService;

    @Override
    @Transactional(readOnly = true)
    public List<Comment> getByBookId(String bookId) throws BookException {
        final Book book = bookService.findById(bookId)
                .orElseThrow(() -> new BookException("Book doesn't exist"));
        return book.getComments();
    }

    @Override
    @Transactional
    public void insert(Comment comment, String bookId) throws CommentException, BookException {
        if (comment == null || StringUtils.isEmpty(comment.getComment())) {
            throw new CommentException("Comment mustn't be null!");
        }
        if (bookId == null) {
            throw new CommentException("Comment must belong to some book!");
        }
        final Book book = bookService.findById(bookId)
                .orElseThrow(() -> new CommentException("Book doesn't exist"));
        book.getComments().add(comment);
        bookService.save(book);
    }

    @Override
    @Transactional
    public void insert(String bookId, String author, String comment) throws CommentException, BookException {
        final Comment newComment = new Comment(null, comment, author);
        insert(newComment, bookId);
    }

    @Override
    @Transactional
    public void deleteById(long id, String bookId) throws CommentException, BookException {
        final Book book = bookService.findById(bookId)
                .orElseThrow(() -> new CommentException("Book doesn't exist"));
        book.getComments().removeIf(c -> c.getId().equals(id));
        bookService.save(book);
    }
}
