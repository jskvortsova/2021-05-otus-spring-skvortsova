package ru.otus.spring.writer.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.spring.entity.Author;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.writer.EntityWriter;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AuthorWriter implements EntityWriter<Author> {
    private final InOutService inOutService;

    @Override
    public void write(Optional<Author> entity) {
        if (entity.isEmpty()) {
            return;
        }
        final Author author = entity.get();
        inOutService.write(String.format("%s. %s", author.getId(), author.getName()));
        inOutService.write("=================================================");
    }
}
