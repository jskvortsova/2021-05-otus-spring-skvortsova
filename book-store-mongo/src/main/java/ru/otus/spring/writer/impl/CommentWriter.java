package ru.otus.spring.writer.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.writer.EntityWriter;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CommentWriter implements EntityWriter<Comment> {
    private final InOutService inOutService;

    @Override
    public void write(Optional<Comment> entity) {
        if (entity.isEmpty()) {
            return;
        }
        final Comment comment = entity.get();
        inOutService.write(String.format("Author: %s", comment.getAuthor()));
        inOutService.write(String.format("-- %s", comment.getComment()));
        inOutService.write("=================================================");
    }
}
