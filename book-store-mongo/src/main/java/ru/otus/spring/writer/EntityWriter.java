package ru.otus.spring.writer;

import java.util.Optional;

public interface EntityWriter<T> {
    void write(Optional<T> entity);
}
