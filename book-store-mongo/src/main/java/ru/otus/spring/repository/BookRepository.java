package ru.otus.spring.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.otus.spring.entity.Book;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends MongoRepository<Book, String> {

    Optional<Book> findByName(String name);

    @Query("{'authors.name' : :#{#authorName}}")
    List<Book> findByAuthorName(@Param("authorName") String authorName);

    @Query("{'genres' : :#{#genre}}")
    List<Book> findByGenre(@Param("genre") String genre);

    @Query(value = "{'genres' : :#{#genre}}", delete = true)
    List<Book> deleteByGenre(@Param("genre") String genre);

    @Query(value = "{'authors.id' : :#{#authorId}}", delete = true)
    List<Book> deleteByAuthorId(@Param("authorId") String authorId);
}
