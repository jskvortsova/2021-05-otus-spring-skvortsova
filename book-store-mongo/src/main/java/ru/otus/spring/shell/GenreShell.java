package ru.otus.spring.shell;

import lombok.RequiredArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.util.CollectionUtils;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.exception.GenreException;
import ru.otus.spring.service.GenreService;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.writer.EntityWriter;

import java.util.List;
import java.util.Optional;

@ShellComponent
@RequiredArgsConstructor
public class GenreShell {
    private final GenreService genreService;
    private final EntityWriter<Genre> genreWriter;
    private final InOutService inOutService;

    @ShellMethod(key = "3", value = "Press '3' too see available commands")
    public void start() {
        inOutService.write("31 - count all genres");
        inOutService.write("32 - find all genres");
        inOutService.write("33 {id} - find by id");
        inOutService.write("34 {name} - insert new genre");
        inOutService.write("35 {id} - delete by ID");
        inOutService.write("==================================");
        inOutService.write("0 - main menu");
        inOutService.write("exit - exit");
    }

    @ShellMethod(key = "31", value = "Count all genres")
    public void count() {
        inOutService.write(String.format("Count: %d", genreService.count()));
    }

    @ShellMethod(key = "32", value = "Find all genres")
    public void findAll() {
        printResult(genreService.getAll());
    }

    @ShellMethod(key = "33", value = "Find genre by ID")
    public void findById(String id) {
        final Optional<Genre> genre = genreService.findById(id);
        genreWriter.write(genre);
    }

    @ShellMethod(key = "34", value = "Insert new genre")
    public void insert(String name) {
        try {
            genreService.insert(name);
        } catch (GenreException e) {
            inOutService.write("Error inserting genre. " + e.getMessage());
        }
    }

    @ShellMethod(key = "35", value = "Delete genre by ID")
    public void delete(String id) {
        genreService.deleteById(id);
    }

    private void printResult(List<Genre> genres) {
        if (CollectionUtils.isEmpty(genres)) {
            inOutService.write("Жанры не найдены");
        } else {
            genres.forEach(genre -> genreWriter.write(Optional.ofNullable(genre)));
        }
    }
}
