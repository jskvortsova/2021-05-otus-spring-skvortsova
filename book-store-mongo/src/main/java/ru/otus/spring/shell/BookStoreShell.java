package ru.otus.spring.shell;

import lombok.RequiredArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring.service.InOutService;

@ShellComponent
@RequiredArgsConstructor
public class BookStoreShell {
    private final InOutService inOutService;

    @ShellMethod(key = "start", value = "Press 'start' too see available commands")
    public void start() {
        printMainMenu();
    }

    @ShellMethod(key = "0", value = "Count all books")
    public void mainMenu() {
        printMainMenu();
    }

    private void printMainMenu() {
        inOutService.write("1 - books menu");
        inOutService.write("2 - authors menu");
        inOutService.write("3 - genres menu");
        inOutService.write("==================================");
        inOutService.write("0 - main menu");
        inOutService.write("exit - exit");
    }
}
