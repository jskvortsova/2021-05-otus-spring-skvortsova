package ru.otus.spring.shell;

import lombok.RequiredArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.util.CollectionUtils;
import ru.otus.spring.entity.Author;
import ru.otus.spring.exception.AuthorException;
import ru.otus.spring.service.AuthorService;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.writer.EntityWriter;

import java.util.List;
import java.util.Optional;

@ShellComponent
@RequiredArgsConstructor
public class AuthorShell {
    private final AuthorService authorService;
    private final EntityWriter<Author> authorWriter;
    private final InOutService inOutService;

    @ShellMethod(key = "2", value = "Press '2' too see available commands")
    public void start() {
        inOutService.write("21 - count all authors");
        inOutService.write("22 - find all authors");
        inOutService.write("23 {id} - find by id");
        inOutService.write("24 {name} - insert new author");
        inOutService.write("25 {id} - delete by ID");
        inOutService.write("==================================");
        inOutService.write("0 - main menu");
        inOutService.write("exit - exit");
    }

    @ShellMethod(key = "21", value = "Count all authors")
    public void count() {
        inOutService.write(String.format("Count: %d", authorService.count()));
    }

    @ShellMethod(key = "22", value = "Find all authors")
    public void findAll() {
        printResult(authorService.getAll());
    }

    @ShellMethod(key = "23", value = "Find author by ID")
    public void findById(String id) {
        final Optional<Author> author = authorService.findById(id);
        authorWriter.write(author);
    }

    @ShellMethod(key = "24", value = "Insert new author")
    public void insert(String name) {
        try {
            authorService.insert(name);
        } catch (AuthorException e) {
            inOutService.write("Error inserting author. " + e.getMessage());
        }
    }

    @ShellMethod(key = "25", value = "Delete author by ID")
    public void delete(String id) {
        authorService.deleteById(id);
    }

    private void printResult(List<Author> authors) {
        if (CollectionUtils.isEmpty(authors)) {
            inOutService.write("Авторы не найдены");
        } else {
            authors.forEach(author -> authorWriter.write(Optional.ofNullable(author)));
        }
    }
}
