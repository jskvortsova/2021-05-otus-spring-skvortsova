package ru.otus.spring.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
@Document(collection = "book")
public class Book {
    @Id
    private String id;
    private String name;
    private String description;
    private List<String> genres;
    private List<Author> authors;
    private List<Comment> comments;

    public Book(String name, String description, List<String> genres, List<Author> authors, List<Comment> comments) {
        this.name = name;
        this.description = description;
        this.genres = genres;
        this.authors = authors;
        this.comments = comments;
    }

    public List<Author> getAuthors() {
        if (authors == null) {
            authors = new ArrayList<>();
        }
        return authors;
    }


}
