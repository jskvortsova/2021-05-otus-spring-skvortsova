package ru.otus.spring.mongock.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import ru.otus.spring.entity.Author;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.repository.BookRepository;
import ru.otus.spring.repository.GenreRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ChangeLog
public class DatabaseChangelog {

    public static final String AUTHOR = "skvortsovaiua";

    @ChangeSet(order = "001", id = "dropDb", author = AUTHOR, runAlways = true)
    public void dropDb(MongoDatabase db) {
        db.drop();
    }

    @ChangeSet(order = "002", id = "insertAuthors", author = AUTHOR)
    public void insertAuthors(MongoDatabase db) {
        MongoCollection<Document> myCollection = db.getCollection("author");
        List<Document> authors = new ArrayList<>();
        authors.add(new Document().append("_id", "1").append("name", "Джоан Роулинг"));
        authors.add(new Document().append("_id", "2").append("name", "Макс Фрай"));
        authors.add(new Document().append("_id", "3").append("name", "Дафна дю Морье"));
        authors.add(new Document().append("_id", "4").append("name", "Еще один автор"));
        myCollection.insertMany(authors);
    }

    @ChangeSet(order = "003", id = "insertGenres", author = AUTHOR)
    public void insertGenres(GenreRepository repository) {
        repository.save(new Genre("триллер"));
        repository.save(new Genre("комедия"));
        repository.save(new Genre("роман"));
        repository.save(new Genre("фэнтези"));
        repository.save(new Genre("приключения"));
    }

    @ChangeSet(order = "004", id = "insertBooks", author = AUTHOR)
    public void insertBooks(BookRepository repository) {
        final Book book1 = new Book("Гарри Поттер", "Сказка о волшебном мальчике",
                Collections.singletonList("приключения"),
                Collections.singletonList(new Author("1", "Джоан Роулинг")),
                Collections.singletonList(new Comment(1L, "Excellent!", "Jane")));
        book1.setId("book1id");
        repository.save(book1);
    }
}
