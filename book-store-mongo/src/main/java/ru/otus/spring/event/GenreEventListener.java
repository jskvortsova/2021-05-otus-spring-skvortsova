package ru.otus.spring.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.stereotype.Component;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.service.BookService;
import ru.otus.spring.service.GenreService;

@Component
public class GenreEventListener extends AbstractMongoEventListener<Genre> {
    @Autowired
    private BookService bookService;

    @Autowired
    private GenreService genreService;

    @Override
    public void onBeforeDelete(BeforeDeleteEvent<Genre> event) {
        super.onBeforeDelete(event);
        genreService.findById(event.getDocument().get("_id").toString())
                .ifPresent(g -> bookService.deleteByGenre(g.getName()));
    }
}
