package ru.otus.spring.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.stereotype.Component;
import ru.otus.spring.entity.Author;
import ru.otus.spring.service.BookService;

@Component
public class AuthorEventListener extends AbstractMongoEventListener<Author> {
    @Autowired
    private BookService bookService;

    @Override
    public void onBeforeDelete(BeforeDeleteEvent<Author> event) {
        super.onBeforeDelete(event);
        bookService.deleteByAuthorId(event.getDocument().get("_id").toString());
    }
}
