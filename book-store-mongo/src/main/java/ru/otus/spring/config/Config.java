package ru.otus.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.service.impl.InOutServiceImpl;

@Configuration
public class Config {
    @Bean
    public InOutService inOutService() {
        return new InOutServiceImpl(System.out, System.in);
    }
}
