package ru.otus.spring.repository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.LocalizedResourceHelper;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import ru.otus.spring.config.QuestionFileConfig;
import ru.otus.spring.exception.QuestionException;
import ru.otus.spring.model.Answer;
import ru.otus.spring.model.Question;
import ru.otus.spring.service.LocaleProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Repository
@RequiredArgsConstructor
public class QuestionRepositoryImpl implements QuestionRepository {
    public static final int QUESTION_INFO_PARTS = 2;
    private final QuestionFileConfig questionFileConfig;
    private final LocaleProvider localeProvider;

    @Override
    public List<Question> findAll() throws QuestionException {
        List<Question> questions = new ArrayList<>();
        final LocalizedResourceHelper localizedResourceHelper = new LocalizedResourceHelper();
        final Resource localizedResource = localizedResourceHelper.findLocalizedResource(questionFileConfig.getFullName(),
                questionFileConfig.getExtension(), Locale.forLanguageTag(localeProvider.getLocale()));

        try (final InputStream resource = localizedResource.getInputStream();
             InputStreamReader streamReader = new InputStreamReader(resource, StandardCharsets.UTF_8);
             final BufferedReader bufferedReader = new BufferedReader(streamReader)) {
            String questionLine;
            while ((questionLine = bufferedReader.readLine()) != null) {
                final List<String> questionInfo = Stream.of(questionLine.split(","))
                        .filter(q -> !StringUtils.isEmpty(q))
                        .collect(Collectors.toList());
                final Question question = parseQuestion(questionInfo);
                if (question != null) {
                    questions.add(question);
                }
            }
        } catch (IOException e) {
            throw new QuestionException("Error parsing question", e);
        }
        return questions;
    }

    private Question parseQuestion(List<String> questionInfo) {
        // If questionInfo < 2 then answer is not defined
        if (questionInfo.size() < QUESTION_INFO_PARTS) {
            return null;
        }
        final Question question = new Question(questionInfo.get(0)); // this is the question

        // process answers
        for (int i = 1; i < questionInfo.size(); i++) {
            final String[] ans = questionInfo.get(i).split(":");
            boolean isCorrect = ans.length > 1 && Boolean.parseBoolean(ans[1]);
            final Answer answer = new Answer(ans[0], isCorrect);
            question.addAnswer(answer);
        }
        return question;
    }
}
