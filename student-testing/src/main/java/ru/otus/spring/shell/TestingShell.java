package ru.otus.spring.shell;

import lombok.RequiredArgsConstructor;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import ru.otus.spring.model.Student;
import ru.otus.spring.model.TestResult;
import ru.otus.spring.service.LocalizationService;
import ru.otus.spring.service.StudentService;
import ru.otus.spring.service.TestingService;

@ShellComponent
@RequiredArgsConstructor
public class TestingShell {
    private final StudentService studentService;
    private final LocalizationService localizationService;
    private final TestingService testingService;

    private Student student;

    @ShellMethod(key = "start", value = "Press 'start' to see available commands")
    public String start() {
        return localizationService.getMessage("command.start.greeting");
    }

    @ShellMethod(key = "login", value = "Login into system")
    public Student login() {
        this.student = studentService.getStudent();
        return student;
    }

    @ShellMethod(key = "test", value = "Press 'test' to start testing")
    @ShellMethodAvailability(value = "isStudentLoggedIn")
    public TestResult test() {
        return testingService.runTest(this.student);
    }

    private Availability isStudentLoggedIn() {
        return student == null
                ? Availability.unavailable(localizationService.getMessage("command.test.ask-for-login"))
                : Availability.available();
    }
}
