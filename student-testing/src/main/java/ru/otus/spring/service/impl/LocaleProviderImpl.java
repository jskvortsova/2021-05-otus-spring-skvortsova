package ru.otus.spring.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.otus.spring.service.LocaleProvider;

@Component
public class LocaleProviderImpl implements LocaleProvider {
    private final String locale;

    public LocaleProviderImpl(@Value("${locale.default}") String locale) {
        this.locale = locale;
    }

    @Override
    public String getLocale() {
        return locale;
    }
}
