package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.otus.spring.exception.QuestionException;
import ru.otus.spring.model.Question;
import ru.otus.spring.repository.QuestionRepository;
import ru.otus.spring.service.MessageService;
import ru.otus.spring.service.QuestionService;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final MessageService messageService;

    @Override
    public int askQuestions() throws QuestionException {
        final List<Question> questions = questionRepository.findAll();
        int score = 0;
        for (Question question : questions) {
            messageService.write(question.getQuestion());
            if (question.getAnswers().size() > 1) {
                question.getAnswers().forEach(a -> messageService.write(a.getAnswer()));
            }
            messageService.writeByKey("input.answer");
            final String enteredAnswer = messageService.read();
            final boolean correct = question.getAnswers().stream()
                    .anyMatch(a -> a.getAnswer().equalsIgnoreCase(enteredAnswer) && a.isCorrect());
            if (correct) {
                score++;
            }
        }
        return score;
    }
}
