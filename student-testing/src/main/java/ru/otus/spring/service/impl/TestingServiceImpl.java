package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.otus.spring.exception.QuestionException;
import ru.otus.spring.model.Student;
import ru.otus.spring.model.TestResult;
import ru.otus.spring.service.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestingServiceImpl implements TestingService {
    private final QuestionService questionService;
    private final TestResultWriter testResultWriter;
    private final MessageService messageService;

    @Override
    public TestResult runTest(Student student) {
        final TestResult testResult = new TestResult(student);
        try {
            testResult.setScore(questionService.askQuestions());
        } catch (QuestionException e) {
            messageService.writeByKey("test.unexpected.error");
            log.error("Error while testing", e);
            return null;
        }

        testResultWriter.write(testResult);
        return testResult;
    }
}
