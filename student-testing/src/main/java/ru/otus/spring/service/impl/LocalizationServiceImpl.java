package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.otus.spring.service.LocaleProvider;
import ru.otus.spring.service.LocalizationService;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class LocalizationServiceImpl implements LocalizationService {
    private final MessageSource messageSource;
    private final LocaleProvider localeProvider;

    @Override
    public String getMessage(String key, Object... args) {
        return messageSource.getMessage(key, args, Locale.forLanguageTag(localeProvider.getLocale()));
    }
}
