package ru.otus.spring.service;

import ru.otus.spring.exception.QuestionException;

public interface QuestionService {
    int askQuestions() throws QuestionException;
}
