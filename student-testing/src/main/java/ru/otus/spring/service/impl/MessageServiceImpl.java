package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.service.LocalizationService;
import ru.otus.spring.service.MessageService;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {
    private final InOutService inOutService;
    private final LocalizationService localizationService;

    @Override
    public void write(String message) {
        inOutService.write(message);
    }

    @Override
    public void writeByKey(String key, Object... args) {
        final String message = localizationService.getMessage(key, args);
        inOutService.write(message);
    }

    @Override
    public String read() {
        return inOutService.read();
    }
}
