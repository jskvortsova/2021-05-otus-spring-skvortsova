package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.otus.spring.model.Student;
import ru.otus.spring.service.MessageService;
import ru.otus.spring.service.StudentService;

@RequiredArgsConstructor
@Service
@Slf4j
public class StudentServiceImpl implements StudentService {
    private final MessageService messageService;

    @Override
    public Student getStudent() {
        final Student student = new Student();
        messageService.writeByKey("input.lastName");
        final String lastName = messageService.read();
        student.setLastName(lastName);

        messageService.writeByKey("input.firstName");
        final String firstName = messageService.read();
        student.setFirstName(firstName);
        return student;
    }
}
