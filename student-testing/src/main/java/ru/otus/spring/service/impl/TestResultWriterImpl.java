package ru.otus.spring.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.otus.spring.model.Student;
import ru.otus.spring.model.TestResult;
import ru.otus.spring.service.MessageService;
import ru.otus.spring.service.TestResultWriter;

@Service
public class TestResultWriterImpl implements TestResultWriter {
    private final MessageService messageService;
    private final int passingScore;

    public TestResultWriterImpl(MessageService messageService,
                                @Value("${passing-score}") int passingScore) {
        this.messageService = messageService;
        this.passingScore = passingScore;
    }

    @Override
    public void write(TestResult testResult) {
        final Student student = testResult.getStudent();
        messageService.writeByKey("greeting",
                student.getFirstName(), student.getLastName());
        messageService.writeByKey("score", testResult.getScore());
        if (testResult.getScore() >= passingScore) {
            messageService.writeByKey("test.passed");
        } else {
            messageService.writeByKey("test.failed");
        }
    }
}
