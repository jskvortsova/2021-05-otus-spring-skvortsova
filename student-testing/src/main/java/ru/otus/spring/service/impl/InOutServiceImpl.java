package ru.otus.spring.service.impl;

import ru.otus.spring.service.InOutService;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class InOutServiceImpl implements InOutService {
    private final PrintStream out;
    private final Scanner scanner;

    public InOutServiceImpl(PrintStream out, InputStream in) {
        this.out = out;
        this.scanner = new Scanner(in);
    }

    @Override
    public void write(String message) {
        out.println(message);
    }

    @Override
    public String read() {
        return scanner.nextLine();
    }
}
