package ru.otus.spring.service;

public interface LocaleProvider {
    String getLocale();
}
