package ru.otus.spring.service;

public interface MessageService {
    void write(String message);

    void writeByKey(String key, Object... args);

    String read();
}
