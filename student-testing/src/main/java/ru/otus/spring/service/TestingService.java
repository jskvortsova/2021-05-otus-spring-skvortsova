package ru.otus.spring.service;

import ru.otus.spring.model.Student;
import ru.otus.spring.model.TestResult;

public interface TestingService {
    TestResult runTest(Student student);
}
