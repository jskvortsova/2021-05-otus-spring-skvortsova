package ru.otus.spring.service;

import ru.otus.spring.model.TestResult;

public interface TestResultWriter {
    void write(TestResult testResult);
}
