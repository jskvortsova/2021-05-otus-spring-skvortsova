package ru.otus.spring.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "questions")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class QuestionFileConfig {
    private String path;
    private String name;
    private String extension;

    public String getFullName() {
        return String.format("%s%s", path, name);
    }

    public String getExtension() {
        return String.format(".%s", extension);
    }
}
