package ru.otus.spring.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TestResult {
    private Student student;
    private int score;

    public TestResult(Student student) {
        this.student = student;
    }
}
