package ru.otus.spring.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Question {
    private String question;
    private List<Answer> answers;

    public Question(String question) {
        this.question = question;
        answers = new ArrayList<>();
    }

    public void addAnswer(Answer answer) {
        answers.add(answer);
    }
}
