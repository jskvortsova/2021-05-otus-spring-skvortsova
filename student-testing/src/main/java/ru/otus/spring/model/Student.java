package ru.otus.spring.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Student {
    private String lastName;
    private String firstName;
}
