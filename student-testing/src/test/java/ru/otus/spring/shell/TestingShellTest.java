package ru.otus.spring.shell;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.shell.CommandNotCurrentlyAvailable;
import org.springframework.shell.Shell;
import ru.otus.spring.model.Student;
import ru.otus.spring.model.TestResult;
import ru.otus.spring.service.LocalizationService;
import ru.otus.spring.service.StudentService;
import ru.otus.spring.service.TestingService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
class TestingShellTest {

    public static final String START_COMMAND = "start";
    public static final String LOGIN_COMMAND = "login";
    public static final String TEST_COMMAND = "test";

    @MockBean
    private LocalizationService localizationService;

    @MockBean
    private StudentService studentService;

    @MockBean
    private TestingService testingService;

    @Autowired
    private Shell shell;

    @Test
    void start() {
        final String greeting = "greeting";
        when(localizationService.getMessage("command.start.greeting"))
                .thenReturn(greeting);
        final String res = (String) shell.evaluate(() -> START_COMMAND);
        assertEquals(greeting, res);
    }

    @Test
    void login() {
        final Student expectedStudent = getStudent();

        final Student actualStudent = (Student) shell.evaluate(() -> LOGIN_COMMAND);
        assertEquals(expectedStudent.toString(), actualStudent.toString());
    }

    @Test
    void test() {
        final String loginMessage = "Login!";
        when(localizationService.getMessage("command.test.ask-for-login"))
                .thenReturn(loginMessage);
        final CommandNotCurrentlyAvailable evaluate = (CommandNotCurrentlyAvailable) shell.evaluate(() -> TEST_COMMAND);
        assertTrue(evaluate.getMessage().contains(loginMessage));
    }

    @Test
    void test2() {
        final Student student = getStudent();
        final int expectedScore = 3;
        final TestResult expectedTestResult = new TestResult();
        expectedTestResult.setStudent(student);
        expectedTestResult.setScore(expectedScore);
        when(testingService.runTest(student)).thenReturn(expectedTestResult);

        shell.evaluate(() -> LOGIN_COMMAND);
        TestResult testResult = (TestResult) shell.evaluate(() -> TEST_COMMAND);
        assertEquals(student.toString(), testResult.getStudent().toString());
        assertEquals(expectedScore, testResult.getScore());
    }

    private Student getStudent() {
        final Student expectedStudent = new Student();
        expectedStudent.setFirstName("Ivan");
        expectedStudent.setLastName("Ivanov");

        when(studentService.getStudent()).thenReturn(expectedStudent);

        return expectedStudent;
    }
}