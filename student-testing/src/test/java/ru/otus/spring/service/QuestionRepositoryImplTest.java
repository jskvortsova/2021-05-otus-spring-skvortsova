package ru.otus.spring.service;

import org.junit.jupiter.api.Test;
import ru.otus.spring.config.QuestionFileConfig;
import ru.otus.spring.exception.QuestionException;
import ru.otus.spring.model.Answer;
import ru.otus.spring.model.Question;
import ru.otus.spring.repository.QuestionRepositoryImpl;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class QuestionRepositoryImplTest {

    @Test
    void testQuestions() throws QuestionException {
        final QuestionFileConfig questionFileConfig = new QuestionFileConfig("", "questions", "csv");
        final LocaleProvider localeProvider = mock(LocaleProvider.class);
        when(localeProvider.getLocale()).thenReturn("ru-RU");
        final QuestionRepositoryImpl questionService = new QuestionRepositoryImpl(questionFileConfig, localeProvider);
        final List<Question> questions = questionService.findAll();
        assertNotNull(questions);
        assertEquals(1, questions.size());
        final Question question = questions.get(0);
        assertEquals("Test question.", question.getQuestion());
        final List<Answer> answers = question.getAnswers();
        assertNotNull(answers);
        assertEquals(2, answers.size());
        assertEquals("answer1", answers.get(0).getAnswer());
        assertTrue(answers.get(0).isCorrect());
        assertEquals("answer2", answers.get(1).getAnswer());
        assertFalse(answers.get(1).isCorrect());
    }
}