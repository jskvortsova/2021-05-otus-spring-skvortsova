package ru.otus.spring.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.otus.spring.exception.QuestionException;
import ru.otus.spring.model.Answer;
import ru.otus.spring.model.Question;
import ru.otus.spring.repository.QuestionRepository;
import ru.otus.spring.service.impl.QuestionServiceImpl;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;

class QuestionServiceImplTest {
    private QuestionService questionService;
    private MessageService messageService;

    @BeforeEach
    void setup() throws QuestionException {
        QuestionRepository questionRepository = Mockito.mock(QuestionRepository.class);
        this.messageService = Mockito.mock(MessageService.class);
        this.questionService = new QuestionServiceImpl(questionRepository, messageService);

        final Question q1 = new Question("Q1");
        q1.addAnswer(new Answer("a1", true));
        q1.addAnswer(new Answer("a2", false));

        final Question q2 = new Question("Q2");
        q2.addAnswer(new Answer("qa1", false));
        q2.addAnswer(new Answer("qa2", true));

        when(questionRepository.findAll()).thenReturn(Stream.of(q1, q2).collect(Collectors.toList()));
    }

    @Test
    void askQuestions() throws QuestionException {
        when(messageService.read()).thenReturn("a1");
        final int score = questionService.askQuestions();
        Assertions.assertEquals(1, score);

        when(messageService.read()).thenReturn("qa2");
        final int score2 = questionService.askQuestions();
        Assertions.assertEquals(1, score2);

        when(messageService.read()).thenReturn("wrong");
        final int score3 = questionService.askQuestions();
        Assertions.assertEquals(0, score3);
    }
}