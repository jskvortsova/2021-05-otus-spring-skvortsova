insert into genre (id, name) values (1, 'триллер');
insert into genre (id, name) values (2, 'комедия');
insert into genre (id, name) values (3, 'роман');
insert into genre (id, name) values (4, 'фэнтези');
insert into genre (id, name) values (5, 'приключения');

insert into book (id, name, description, genre_id) values (1, 'Гарри Поттер', 'Сказка о волшебном мальчике', 4);
insert into book (id, name, description, genre_id) values (2, 'Чужак', 'Если вы взяли в руки эту книгу, будьте готовы отправиться в самое захватывающее путешествие своей жизни, полное опасностей и приключений.', 5);
insert into book (id, name, description, genre_id) values (3, 'Ребекка', 'Юная компаньонка капризной пожилой американки становится женой импозантного английского аристократа Максимилиана де Уинтера, терзаемого тайной печалью, и прибывает вместе с ним в его родовое поместье Мэндерли.', 3);
insert into book (id, name, description, genre_id) values (4, 'Test', 'Test book.', 3);

insert into author (id, name) values (1, 'Джоан Роулинг');
insert into author (id, name) values (2, 'Макс Фрай');
insert into author (id, name) values (3, 'Дафна дю Морье');

insert into book_author (id, book_id, author_id) values (1, 1, 1);
insert into book_author (id, book_id, author_id) values (2, 2, 2);
insert into book_author (id, book_id, author_id) values (3, 3, 3);

insert into comment (book_id, author, comment) values (1, 'Borya', 'Excellent!');