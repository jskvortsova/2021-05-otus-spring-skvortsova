package ru.otus.spring.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.exception.CommentException;
import ru.otus.spring.repository.CommentRepository;
import ru.otus.spring.service.CommentService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class CommentServiceImplTest {

    private CommentService commentService;
    private static final long EXPECTED_COUNT = 1;

    @BeforeEach
    void setup() {
        final CommentRepository commentRepository = Mockito.mock(CommentRepository.class);
        commentService = new CommentServiceImpl(commentRepository);
        when(commentRepository.count()).thenReturn(EXPECTED_COUNT);

        final Book book = new Book();
        book.setId(1L);
        final Comment comment = new Comment(1L, book, "comment", "author");
        when(commentRepository.findById(1L)).thenReturn(java.util.Optional.of(comment));
        when(commentRepository.findAll()).thenReturn(Collections.singletonList(comment));
        when(commentRepository.getByBookId(1L)).thenReturn(Collections.singletonList(comment));
    }

    @Test
    void count() {
        assertEquals(EXPECTED_COUNT, commentService.count());
    }

    @Test
    void getAll() {
        final List<Comment> all = commentService.getAll();
        assertEquals(EXPECTED_COUNT, all.size());
    }

    @Test
    void findById() {
        final Optional<Comment> comment = commentService.findById(1L);
        assertThat(comment).isPresent();
        assertEquals(1L, comment.get().getId());
    }

    @Test
    void getByBookId() {
        final List<Comment> comments = commentService.getByBookId(1L);
        assertNotNull(comments);
        assertEquals(1, comments.size());
        final Comment comment = comments.get(0);
        final Book book = comment.getBook();
        assertNotNull(book);
        assertEquals(1L, book.getId());
    }

    @Test
    void insert() {
        assertThrows(CommentException.class, () -> commentService.insert(null));
        assertThrows(CommentException.class, () -> commentService.insert(new Comment()));
        assertThrows(CommentException.class, () -> commentService.insert(new Comment(null, null, "comment", "author")));
        assertThrows(CommentException.class, () -> commentService.insert(new Comment(null, new Book(), "comment", "author")));
        final Book book = new Book();
        book.setId(123L);
        assertThrows(CommentException.class, () -> commentService.insert(new Comment(null, book, null, "author")));
        assertDoesNotThrow(() -> commentService.insert(new Comment(null, book, "comment", null)));
        assertDoesNotThrow(() -> commentService.insert(new Comment(null, book, "comment", "author")));
    }

    @Test
    void testInsert() {
        assertThrows(CommentException.class, () -> commentService.insert(0, "author", "comment"));
        assertThrows(CommentException.class, () -> commentService.insert(1, "author", null));
        assertDoesNotThrow(() -> commentService.insert(1, null, "comment"));
        assertDoesNotThrow(() -> commentService.insert(1, "author", "comment"));
    }

    @Test
    void deleteById() {
        assertDoesNotThrow(() -> commentService.deleteById(1L));
    }
}