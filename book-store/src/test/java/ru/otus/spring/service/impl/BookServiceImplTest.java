package ru.otus.spring.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.exception.GenreException;
import ru.otus.spring.repository.BookRepository;
import ru.otus.spring.service.AuthorService;
import ru.otus.spring.service.BookService;
import ru.otus.spring.service.GenreService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BookServiceImplTest {
    private static final long EXPECTED_COUNT = 4;
    private BookService bookService;
    private List<Book> expectedBooks;

    @BeforeEach
    void setUp() throws GenreException {
        final BookRepository bookRepository = mock(BookRepository.class);
        final GenreService genreService = mock(GenreService.class);
        final AuthorService authorService = mock(AuthorService.class);
        bookService = new BookServiceImpl(bookRepository, genreService, authorService);
        when(bookRepository.count()).thenReturn(EXPECTED_COUNT);
        final Book test1 = createBook(1L, "Test1", 1);
        final Book test2 = createBook(2L, "Test2", 2);
        final Book test3 = createBook(3L, "Test3", 2);
        final Book test4 = createBook(4L, "Test4", 1);
        expectedBooks = Stream.of(test1, test2, test3, test4).collect(toList());
        when(bookRepository.findAll()).thenReturn(expectedBooks);
        when(bookRepository.findById(1L)).thenReturn(Optional.of(test1));
        when(bookRepository.findByName("Test1")).thenReturn(Optional.of(test1));
        when(bookRepository.findByAuthorId(1)).thenReturn(Collections.singletonList(test1));
        when(bookRepository.findByGenreId(1)).thenReturn(Stream.of(test1, test4).collect(toList()));

        when(genreService.findById(1)).thenReturn(Optional.of(new Genre(1L, "1")));
        when(genreService.findById(2)).thenReturn(Optional.of(new Genre(2L, "2")));
        when(genreService.findById(3)).thenReturn(Optional.of(new Genre(3L, "3")));
    }

    @Test
    void count() {
        assertEquals(EXPECTED_COUNT, bookService.count());
    }

    @Test
    void getAll() {
        final List<Book> all = bookService.getAll();
        assertEquals(EXPECTED_COUNT, all.size());
        assertThat(all).doesNotContainNull();
        assertThat(all).isSameAs(expectedBooks);
    }

    @Test
    void findById() {
        final Optional<Book> bookOpt = bookService.findById(1);
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        assertEquals(1, book.getId());
        assertEquals("Test1", book.getName());
    }

    @Test
    void findByName() {
        final Optional<Book> bookOpt = bookService.findByName("Test1");
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        assertEquals(1, book.getId());
        assertEquals("Test1", book.getName());
    }

    @Test
    void findByAuthorId() {
        final List<Book> books = bookService.findByAuthorId(1);
        assertEquals(1, books.size());
        final Book book = books.get(0);
        assertNotNull(book);
        assertEquals(1, book.getId());
        assertEquals("Test1", book.getName());
    }

    @Test
    void findByGenreId() {
        final List<Book> books = bookService.findByGenreId(1);
        assertEquals(2, books.size());
        final Book book = books.get(0);
        assertNotNull(book);
        assertEquals(1, book.getId());
        assertEquals("Test1", book.getName());
    }

    @Test
    void insert() {
        assertThrows(BookException.class, () -> bookService.insert(null, "desc", 4L));
        assertThrows(BookException.class, () -> bookService.insert("name", "desc", null));
        assertDoesNotThrow(() -> bookService.insert("name", "desc", 3L));
    }

    @Test
    void testInsert() {
        assertThrows(BookException.class, () -> bookService.insert(null));
        final Book book1 = new Book().setId(1L).setDescription("desc").setGenre(new Genre(4L));
        assertThrows(BookException.class, () -> bookService.insert(book1));
        final Book book2 = new Book().setId(1L).setName("name").setDescription("desc");
        assertThrows(BookException.class, () -> bookService.insert(book2));
        final Book book3 = new Book().setId(1L).setName("name").setDescription("desc").setGenre(new Genre(3L));
        assertDoesNotThrow(() -> bookService.insert(book3));

    }

    @Test
    void deleteById() {
        assertDoesNotThrow(() -> bookService.deleteById(4));
    }

    private Book createBook(long id, String name, long genreId) {
        return new Book().setId(id).setName(name).setDescription("Test").setGenre(new Genre(genreId));
    }
}