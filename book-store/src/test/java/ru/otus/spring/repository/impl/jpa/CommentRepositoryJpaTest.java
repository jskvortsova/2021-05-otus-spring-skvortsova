package ru.otus.spring.repository.impl.jpa;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.repository.BookRepository;
import ru.otus.spring.repository.CommentRepository;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class CommentRepositoryJpaTest {
    @Autowired
    private CommentRepository commentRepositoryJpa;
    @Autowired
    private BookRepository bookRepositoryJpa;

    private static final long EXPECTED_COUNT = 1;

    @Test
    void count() {
        final Long actualCount = commentRepositoryJpa.count();
        assertEquals(EXPECTED_COUNT, actualCount);
    }

    @Test
    void getAll() {
        final List<Comment> comments = commentRepositoryJpa.findAll();
        assertNotNull(comments);
        assertEquals(EXPECTED_COUNT, comments.size());
    }

    @Test
    void findById() {
        final long id = 1;
        final Optional<Comment> commentOpt = commentRepositoryJpa.findById(id);
        assertThat(commentOpt).isPresent();
        final Comment comment = commentOpt.get();
        assertEquals(id, comment.getId());
        assertEquals("Borya", comment.getAuthor());
    }

    @Test
    void getByBookId() {
        final long bookId = 1;
        final List<Comment> comments = commentRepositoryJpa.getByBookId(bookId);
        assertNotNull(comments);
        assertEquals(1, comments.size());
        final Comment comment = comments.get(0);
        assertNotNull(comment.getBook());
        assertEquals(1, comment.getBook().getId());
    }

    @Test
    void insert() {
        assertEquals(EXPECTED_COUNT, commentRepositoryJpa.count());
        final long bookId = 1L;
        final Optional<Book> bookOpt = bookRepositoryJpa.findById(bookId);
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        commentRepositoryJpa.save(new Comment(null, book, "comment", "author"));
        assertEquals(EXPECTED_COUNT + 1, commentRepositoryJpa.count());
        final List<Comment> comments = commentRepositoryJpa.getByBookId(bookId);
        assertEquals(2, comments.size());
        final Optional<Comment> insertedComment = comments.stream()
                .filter(comment -> comment.getComment().equalsIgnoreCase("comment")
                        && comment.getAuthor().equalsIgnoreCase("author"))
                .findFirst();
        assertTrue(insertedComment.isPresent());
        final Long commentId = insertedComment.get().getId();
        commentRepositoryJpa.save(new Comment(commentId, book, "new comment", "author"));
        final List<Comment> comments2 = commentRepositoryJpa.getByBookId(bookId);
        assertEquals(2, comments2.size());
        final Optional<Comment> updatedComment = comments2.stream().filter(comment ->
                comment.getComment().equalsIgnoreCase("new comment")
                        && comment.getAuthor().equalsIgnoreCase("author")
                        && comment.getId().equals(commentId))
                .findFirst();
        assertTrue(updatedComment.isPresent());
    }

    @Test
    void deleteById() {
        final long id = 1;
        final Optional<Comment> comment = commentRepositoryJpa.findById(id);
        assertThat(comment).isPresent();
        commentRepositoryJpa.deleteById(id);
        final Optional<Comment> deleted = commentRepositoryJpa.findById(id);
        assertThat(deleted).isEmpty();
    }
}