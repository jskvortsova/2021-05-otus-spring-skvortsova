package ru.otus.spring.repository.impl.jpa;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.repository.BookRepository;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class BookRepositoryJpaTest {
    public static final String EXPECTED_BOOK_NAME = "Чужак";
    public static final int EXPECTED_COUNT = 4;
    @Autowired
    private BookRepository bookRepositoryJpa;
    @Autowired
    private TestEntityManager em;

    @Test
    void count() {
        assertEquals(EXPECTED_COUNT, bookRepositoryJpa.count());
    }

    @Test
    void getAll() {
        final List<Book> all = bookRepositoryJpa.findAll();
        assertEquals(EXPECTED_COUNT, all.size());
        final Book book = all.get(0);
        assertEquals(1, book.getId());
        assertEquals("Гарри Поттер", book.getName());
        assertEquals(4, book.getGenre().getId());
    }

    @Test
    void findById() {
        final Optional<Book> bookOpt = bookRepositoryJpa.findById(2L);
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        assertEquals(2, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertEquals(5, book.getGenre().getId());
    }

    @Test
    void findByName() {
        final Optional<Book> bookOpt = bookRepositoryJpa.findByName(EXPECTED_BOOK_NAME);
        assertThat(bookOpt).isPresent();
        final Book book = bookOpt.get();
        assertEquals(2, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertEquals(5, book.getGenre().getId());
    }

    @Test
    void findByAuthorId() {
        final List<Book> books = bookRepositoryJpa.findByAuthorId(2);
        assertEquals(1, books.size());
        final Book book = books.get(0);
        assertEquals(2, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertEquals(5, book.getGenre().getId());
    }

    @Test
    void findByGenreId() {
        final List<Book> books = bookRepositoryJpa.findByGenreId(5);
        assertEquals(1, books.size());
        final Book book = books.get(0);
        assertEquals(2, book.getId());
        assertEquals(EXPECTED_BOOK_NAME, book.getName());
        assertEquals(5, book.getGenre().getId());
    }

    @Test
    void insert() {
        assertEquals(EXPECTED_COUNT, bookRepositoryJpa.count());
        final Optional<Genre> genre = Optional.ofNullable(em.find(Genre.class, 3L));
        assertThat(genre).isPresent();
        final Book book = new Book().setName("New book").setDescription("New book description").setGenre(genre.get());
        bookRepositoryJpa.save(book);
        assertNotNull(book.getId());
        assertEquals(EXPECTED_COUNT + 1, bookRepositoryJpa.count());
        final Optional<Book> newBookOpt = bookRepositoryJpa.findById(book.getId());
        assertTrue(newBookOpt.isPresent());
        final Book newBook = newBookOpt.get();
        assertEquals(genre.get().getId(), newBook.getGenre().getId());
        assertEquals("New book", newBook.getName());
    }

    @Test
    void deleteById() {
        final long bookId = 1;
        assertEquals(EXPECTED_COUNT, bookRepositoryJpa.count());
        final List<Comment> commentsBeforeDeletion = getCommentByBookId(bookId);
        assertFalse(commentsBeforeDeletion.isEmpty());
        final Book book = bookRepositoryJpa.getById(bookId);
        assertNotNull(book);
        final Optional<Genre> genre = Optional.ofNullable(em.find(Genre.class, book.getGenre().getId()));
        assertThat(genre).isPresent();
        bookRepositoryJpa.deleteById(bookId);
        assertEquals(EXPECTED_COUNT - 1, bookRepositoryJpa.count());
        final Optional<Book> deleted = bookRepositoryJpa.findById(bookId);
        assertFalse(deleted.isPresent());
        final List<Comment> comments = getCommentByBookId(bookId);
        assertThat(comments).isEmpty();
    }


    public List<Comment> getCommentByBookId(long bookId) {
        final TypedQuery<Comment> query = em.getEntityManager().createQuery("select c from Comment c " +
                "join c.book b " +
                "where b.id = :bookId", Comment.class);
        query.setParameter("bookId", bookId);
        return query.getResultList();
    }
}