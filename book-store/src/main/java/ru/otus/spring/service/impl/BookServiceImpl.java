package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.otus.spring.entity.Author;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.exception.AuthorException;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.exception.GenreException;
import ru.otus.spring.repository.BookRepository;
import ru.otus.spring.service.AuthorService;
import ru.otus.spring.service.BookService;
import ru.otus.spring.service.GenreService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final GenreService genreService;
    private final AuthorService authorService;

    @Override
    @Transactional(readOnly = true)
    public long count() {
        return bookRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Book> findById(long id) {
        return bookRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Book> findByName(String name) {
        return bookRepository.findByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Book> findByAuthorId(long authorId) {
        return bookRepository.findByAuthorId(authorId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Book> findByGenreId(long genreId) {
        return bookRepository.findByGenreId(genreId);
    }

    @Override
    @Transactional
    public void insert(Book book) throws BookException {
        if (book == null) {
            throw new BookException("Book mustn't be null");
        }
        if (!StringUtils.hasLength(book.getName())) {
            throw new BookException("Name shouldn't be null or empty");
        }
        if (book.getGenre() == null || book.getGenre().getId() == null) {
            throw new BookException("Genre ID shouldn't be null");
        }
        bookRepository.save(book);
    }

    @Override
    @Transactional
    public void insert(String name, String description, Long genreId) throws BookException, GenreException {
        final Genre genre = getGenre(genreId);
        final Book book = new Book()
                .setName(name)
                .setDescription(description)
                .setGenre(genre);
        insert(book);
    }

    @Override
    public void insert(String name, String description, Long genreId, Long authorId) throws BookException,
            AuthorException, GenreException {
        final Author author = getAuthor(authorId);
        final Genre genre = getGenre(genreId);

        final Book book = new Book()
                .setName(name)
                .setDescription(description)
                .setGenre(genre)
                .setAuthors(Collections.singletonList(author));
        insert(book);
    }

    @Override
    @Transactional
    public void deleteById(long id) {
        bookRepository.deleteById(id);
    }


    private Genre getGenre(Long genreId) throws BookException, GenreException {
        if (genreId == null) {
            throw new BookException("Genre ID mustn't be null!");
        }
        return genreService.findById(genreId)
                .orElseThrow(() -> new BookException("Genre doesn't exist!"));
    }

    private Author getAuthor(Long authorId) throws BookException, AuthorException {
        if (authorId == null) {
            throw new BookException("Author ID mustn't be null!");
        }
        return authorService.findById(authorId)
                .orElseThrow(() -> new BookException("Author doesn't exist!"));
    }
}
