package ru.otus.spring.service;

import ru.otus.spring.entity.Book;
import ru.otus.spring.exception.AuthorException;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.exception.GenreException;

import java.util.List;
import java.util.Optional;

public interface BookService {
    long count();

    List<Book> getAll();

    Optional<Book> findById(long id);

    Optional<Book> findByName(String name);

    List<Book> findByAuthorId(long authorId);

    List<Book> findByGenreId(long genreId);

    void insert(Book book) throws BookException;

    void insert(String name, String description, Long genreId) throws BookException, GenreException;

    void insert(String name, String description, Long genreId, Long authorId) throws BookException, AuthorException, GenreException;

    void deleteById(long id);
}
