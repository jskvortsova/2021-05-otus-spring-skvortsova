package ru.otus.spring.service.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.spring.repository.CommentRepository;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.exception.CommentException;
import ru.otus.spring.service.CommentService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;

    @Override
    @Transactional(readOnly = true)
    public long count() {
        return commentRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Comment> getAll() {
        return commentRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Comment> findById(long id) {
        return commentRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Comment> getByBookId(long bookId) {
        return commentRepository.getByBookId(bookId);
    }

    @Override
    @Transactional
    public void insert(Comment comment) throws CommentException {
        if (comment == null || StringUtils.isEmpty(comment.getComment())) {
            throw new CommentException("Comment mustn't be null!");
        }
        if (comment.getBook() == null || comment.getBook().getId() == null || comment.getBook().getId() <= 0) {
            throw new CommentException("Comment must belong to some book!");
        }
        commentRepository.save(comment);
    }

    @Override
    @Transactional
    public void insert(long bookId, String author, String comment) throws CommentException {
        final Book book = new Book();
        book.setId(bookId);
        final Comment newComment = new Comment(null, book, comment, author);
        insert(newComment);
    }

    @Override
    @Transactional
    public void deleteById(long id) {
        commentRepository.deleteById(id);
    }
}
