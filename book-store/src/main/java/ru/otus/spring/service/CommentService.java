package ru.otus.spring.service;

import ru.otus.spring.entity.Comment;
import ru.otus.spring.exception.CommentException;

import java.util.List;
import java.util.Optional;

public interface CommentService {

    long count();

    List<Comment> getAll();

    Optional<Comment> findById(long id);

    List<Comment> getByBookId(long bookId);

    void insert(Comment comment) throws CommentException;

    void insert(long bookId, String author, String comment) throws CommentException;

    void deleteById(long id);
}
