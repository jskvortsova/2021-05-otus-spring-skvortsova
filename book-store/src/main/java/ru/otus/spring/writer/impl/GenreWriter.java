package ru.otus.spring.writer.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.spring.entity.Author;
import ru.otus.spring.entity.Genre;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.writer.EntityWriter;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class GenreWriter implements EntityWriter<Genre> {
    private final InOutService inOutService;

    @Override
    public void write(Optional<Genre> entity) {
        if(entity.isEmpty()){
            return;
        }
        final Genre genre = entity.get();
        inOutService.write(String.format("%d. %s", genre.getId(), genre.getName()));
        inOutService.write("=================================================");
    }
}
