package ru.otus.spring.writer.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.spring.entity.Author;
import ru.otus.spring.entity.Book;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.writer.EntityWriter;

import java.util.Optional;

import static java.util.stream.Collectors.joining;

@Component
@RequiredArgsConstructor
public class BookWriter implements EntityWriter<Book> {
    private final InOutService inOutService;
    @Override
    public void write(Optional<Book> entity) {
        if(entity.isEmpty()){
            return;
        }
        final Book book = entity.get();
        inOutService.write(String.format("%d. %s", book.getId(), book.getName()));
        inOutService.write(book.getDescription());
        inOutService.write(String.format("Жанр: %s (%d)", book.getGenre().getName(), book.getGenre().getId()));
        final String authors = book.getAuthors()
                .stream()
                .map(Author::getName)
                .collect(joining(", "));
        inOutService.write(String.format("Авторы: %s", authors));
        inOutService.write("=================================================");
    }
}
