package ru.otus.spring.shell;

import lombok.RequiredArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.util.CollectionUtils;
import ru.otus.spring.entity.Book;
import ru.otus.spring.entity.Comment;
import ru.otus.spring.exception.AuthorException;
import ru.otus.spring.exception.BookException;
import ru.otus.spring.exception.CommentException;
import ru.otus.spring.exception.GenreException;
import ru.otus.spring.service.BookService;
import ru.otus.spring.service.CommentService;
import ru.otus.spring.service.InOutService;
import ru.otus.spring.writer.EntityWriter;

import java.util.List;
import java.util.Optional;

@ShellComponent
@RequiredArgsConstructor
public class BookShell {
    private final BookService bookService;
    private final EntityWriter<Book> bookWriter;
    private final CommentService commentService;
    private final EntityWriter<Comment> commentWriter;
    private final InOutService inOutService;

    @ShellMethod(key = "1", value = "Press '1' too see available commands")
    public void start() {
        inOutService.write("11 - count all books");
        inOutService.write("12 - find all books");
        inOutService.write("13 {id} - find by id");
        inOutService.write("14 {name} - find by name");
        inOutService.write("15 {author_id} - find by author id");
        inOutService.write("16 {genre_id} - find by genre id");
        inOutService.write("17 {name} {description} {genre_id} - insert new book");
        inOutService.write("18 {id} - delete by ID");
        inOutService.write("19 {bookId} - print all book comments");
        inOutService.write("111 {bookId} {author} {comment} - add comment to book");
        inOutService.write("112 {name} {description} {genre_id} {author_id} - insert new book with author");
        inOutService.write("==================================");
        inOutService.write("0 - main menu");
        inOutService.write("exit - exit");
    }

    @ShellMethod(key = "11", value = "Count all books")
    public void countAllBooks() {
        inOutService.write(String.format("Count: %d", bookService.count()));
    }

    @ShellMethod(key = "12", value = "Find all books")
    public void findAllBooks() {
        bookService.getAll().forEach(book -> bookWriter.write(Optional.ofNullable(book)));
    }

    @ShellMethod(key = "13", value = "Find by ID")
    public void findById(long id) {
        final Optional<Book> book = bookService.findById(id);
        printBook(book);
    }

    @ShellMethod(key = "14", value = "Find by name")
    public void findByName(String name) {
        final Optional<Book> book = bookService.findByName(name);
        printBook(book);
    }

    @ShellMethod(key = "15", value = "Find by genre ID")
    public void findByAuthorId(Long authorId) {
        final List<Book> books = bookService.findByAuthorId(authorId);
        printBooks(books);
    }

    @ShellMethod(key = "16", value = "Find by genre ID")
    public void findByGenreId(Long genreId) {
        final List<Book> books = bookService.findByGenreId(genreId);
        printBooks(books);
    }

    @ShellMethod(key = "17", value = "Insert new book")
    public void insert(String name, String description, Long genreId) {
        try {
            bookService.insert(name, description, genreId);
        } catch (BookException | GenreException e) {
            inOutService.write("Error while saving book: " + e);
        }
    }

    @ShellMethod(key = "18", value = "Delete by ID")
    public void delete(Long id) {
        bookService.deleteById(id);
    }

    @ShellMethod(key = "19", value = "Print all comments for book")
    public void printComments(Long id) {
        final List<Comment> comments = commentService.getByBookId(id);
        printComments(comments);
    }

    @ShellMethod(key = "111", value = "Add comment")
    public void addComment(long bookId, String author, String comment) {
        try {
            commentService.insert(bookId, author, comment);
        } catch (CommentException e) {
            inOutService.write("Error while saving comment: " + e.getMessage());
        }
    }


    @ShellMethod(key = "112", value = "Insert new book with author")
    public void insertWithAuthor(String name, String description, Long genreId, Long authorId) {
        try {
            bookService.insert(name, description, genreId, authorId);
        } catch (BookException | AuthorException | GenreException e) {
            inOutService.write("Error while saving book: " + e);
        }
    }

    private void printBooks(List<Book> books) {
        if (CollectionUtils.isEmpty(books)) {
            inOutService.write("Книги не найдены");
        } else {
            books.forEach(book -> bookWriter.write(Optional.ofNullable(book)));
        }
    }

    private void printComments(List<Comment> comments) {
        if (CollectionUtils.isEmpty(comments)) {
            inOutService.write("Комментарии не найдены");
        } else {
            comments.forEach(comment -> commentWriter.write(Optional.ofNullable(comment)));
        }
    }

    private void printBook(Optional<Book> book) {
        if (book.isEmpty()) {
            inOutService.write("Книга не найдена");
        } else {
            bookWriter.write(book);
        }
    }
}
