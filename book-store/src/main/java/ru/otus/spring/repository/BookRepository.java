package ru.otus.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.otus.spring.entity.Book;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {

    Optional<Book> findByName(String name);

    @Override
    @Query(value = "select b from Book b " +
            "join fetch b.genre g " +
            "left join fetch b.authors a")
    List<Book> findAll();

    @Query(value = "select b from Book b " +
            "join fetch b.genre g " +
            "left join b.authors a " +
            "where a.id = :authorId")
    List<Book> findByAuthorId(@Param("authorId") long authorId);

    List<Book> findByGenreId(long genreId);
}
