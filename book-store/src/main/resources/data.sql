insert into genre (name) values ('триллер');
insert into genre (name) values ('комедия');
insert into genre (name) values ('роман');
insert into genre (name) values ('фэнтези');
insert into genre (name) values ('приключения');

insert into book (name, description, genre_id) values ('Гарри Поттер', 'Сказка о волшебном мальчике', 4);
insert into book (name, description, genre_id) values ('Чужак', 'Если вы взяли в руки эту книгу, будьте готовы отправиться в самое захватывающее путешествие своей жизни, полное опасностей и приключений.', 5);
insert into book (name, description, genre_id) values ('Ребекка', 'Юная компаньонка капризной пожилой американки становится женой импозантного английского аристократа Максимилиана де Уинтера, терзаемого тайной печалью, и прибывает вместе с ним в его родовое поместье Мэндерли.', 3);

insert into author (name) values ('Джоан Роулинг');
insert into author (name) values ('Макс Фрай');
insert into author (name) values ('Дафна дю Морье');
insert into author (name) values ('Еще один автор');

insert into book_author (book_id, author_id) values (1, 1);
insert into book_author (book_id, author_id) values (1, 4);
insert into book_author (book_id, author_id) values (2, 2);
insert into book_author (book_id, author_id) values (3, 3);

insert into comment (book_id, author, comment) values (1, 'Borya', 'Excellent!');