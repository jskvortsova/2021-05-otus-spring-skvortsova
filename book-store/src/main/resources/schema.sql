DROP TABLE IF EXISTS genre;
DROP TABLE IF EXISTS author;
DROP TABLE IF EXISTS book;
DROP TABLE IF EXISTS book_author;

create table genre (
    id bigint auto_increment PRIMARY KEY,
    name varchar(50) not null
);

create table book (
    id bigint auto_increment not null,
    name text not null,
    description text,
    genre_id bigint not null,
    PRIMARY KEY (id),
    FOREIGN KEY (genre_id) REFERENCES genre(id)
);

create table author (
    id bigint auto_increment not null,
    name text not null,
    PRIMARY KEY (id)
);

create table book_author (
    id bigint auto_increment not null,
    book_id bigint not null,
    author_id bigint not null,
    PRIMARY KEY (id),
    FOREIGN KEY (book_id) REFERENCES book(id) ON DELETE CASCADE,
    FOREIGN KEY (author_id) REFERENCES author(id) ON DELETE CASCADE
);

create table comment (
    id bigint auto_increment not null,
    book_id bigint not null,
    author varchar(100),
    comment text,
    PRIMARY KEY (id),
    FOREIGN KEY (book_id) REFERENCES book(id) ON DELETE CASCADE
)